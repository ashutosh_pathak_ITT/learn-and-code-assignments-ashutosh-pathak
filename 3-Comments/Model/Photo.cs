using Newtonsoft.Json;

namespace Comments.Model
{
    public class Photo
    {

        [JsonProperty("offset")]
        public string Offset { get; set; }

        [JsonProperty("caption")]
        public string Caption { get; set; }

        [JsonProperty("width")]
        public int Width { get; set; }

        [JsonProperty("height")]
        public int Height { get; set; }

        [JsonProperty("photo-url-1280")]
        public string PhotoUrl1280 { get; set; }

        [JsonProperty("photo-url-500")]
        public string PhotoUrl500 { get; set; }

        [JsonProperty("photo-url-400")]
        public string PhotoUrl400 { get; set; }

        [JsonProperty("photo-url-250")]
        public string PhotoUrl250 { get; set; }

        [JsonProperty("photo-url-100")]
        public string PhotoUrl100 { get; set; }

        [JsonProperty("photo-url-75")]
        public string PhotoUrl75 { get; set; }
    }
}