using System.Collections.Generic;
using Newtonsoft.Json;

namespace Comments.Model
{
    public class RootObjectTumblelog
    {

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("timezone")]
        public string Timezone { get; set; }

        [JsonProperty("cname")]
        public bool Cname { get; set; }

        [JsonProperty("feeds")]
        public IList<object> Feeds { get; set; }
    }
}