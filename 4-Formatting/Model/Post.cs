using System.Collections.Generic;
using Newtonsoft.Json;
using System;

namespace Comments.Model
{
    public class Post
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("url-with-slug")]
        public Uri UrlWithSlug { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("date-gmt")]
        public string DateGmt { get; set; }

        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("bookmarklet")]
        public int Bookmarklet { get; set; }

        [JsonProperty("mobile")]
        public int Mobile { get; set; }

        [JsonProperty("feed-item")]
        public string FeedItem { get; set; }

        [JsonProperty("from-feed-id")]
        public int FromFeedId { get; set; }

        [JsonProperty("unix-timestamp")]
        public int UnixTimestamp { get; set; }

        [JsonProperty("format")]
        public string Format { get; set; }

        [JsonProperty("reblog-key")]
        public string ReblogKey { get; set; }

        [JsonProperty("slug")]
        public string Slug { get; set; }

        [JsonProperty("is-submission")]
        public bool IsSubmission { get; set; }

        [JsonProperty("like-button")]
        public string LikeButton { get; set; }

        [JsonProperty("reblog-button")]
        public string ReblogButton { get; set; }

        [JsonProperty("note-count")]
        public string NoteCount { get; set; }

        [JsonProperty("tumblelog")]
        public PostTumblelog Tumblelog { get; set; }

        [JsonProperty("photo-caption")]
        public string PhotoCaption { get; set; }

        [JsonProperty("width")]
        public int Width { get; set; }

        [JsonProperty("height")]
        public int Height { get; set; }

        [JsonProperty("photo-url-1280")]
        public Uri PhotoUrl1280 { get; set; }

        [JsonProperty("photo-url-500")]
        public Uri PhotoUrl500 { get; set; }

        [JsonProperty("photo-url-400")]
        public Uri PhotoUrl400 { get; set; }

        [JsonProperty("photo-url-250")]
        public Uri PhotoUrl250 { get; set; }

        [JsonProperty("photo-url-100")]
        public Uri PhotoUrl100 { get; set; }

        [JsonProperty("photo-url-75")]
        public Uri PhotoUrl75 { get; set; }

        [JsonProperty("photos")]
        public IList<Photo> Photos { get; set; }

        [JsonProperty("tags")]
        public IList<string> Tags { get; set; }

        [JsonProperty("photo-link-url")]
        public string PhotoLinkUrl { get; set; }

        [JsonProperty("reblogged-from-url")]
        public string RebloggedFromUrl { get; set; }

        [JsonProperty("reblogged-from-name")]
        public string RebloggedFromName { get; set; }

        [JsonProperty("reblogged-from-title")]
        public string RebloggedFromTitle { get; set; }

        [JsonProperty("reblogged_from_avatar_url_512")]
        public string RebloggedFromAvatarUrl512 { get; set; }

        [JsonProperty("reblogged_from_avatar_url_128")]
        public string RebloggedFromAvatarUrl128 { get; set; }

        [JsonProperty("reblogged_from_avatar_url_96")]
        public string RebloggedFromAvatarUrl96 { get; set; }

        [JsonProperty("reblogged_from_avatar_url_64")]
        public string RebloggedFromAvatarUrl64 { get; set; }

        [JsonProperty("reblogged_from_avatar_url_48")]
        public string RebloggedFromAvatarUrl48 { get; set; }

        [JsonProperty("reblogged_from_avatar_url_40")]
        public string RebloggedFromAvatarUrl40 { get; set; }

        [JsonProperty("reblogged_from_avatar_url_30")]
        public string RebloggedFromAvatarUrl30 { get; set; }

        [JsonProperty("reblogged_from_avatar_url_24")]
        public string RebloggedFromAvatarUrl24 { get; set; }

        [JsonProperty("reblogged_from_avatar_url_16")]
        public string RebloggedFromAvatarUrl16 { get; set; }

        [JsonProperty("reblogged-root-url")]
        public string RebloggedRootUrl { get; set; }

        [JsonProperty("reblogged-root-name")]
        public string RebloggedRootName { get; set; }

        [JsonProperty("reblogged-root-title")]
        public string RebloggedRootTitle { get; set; }

        [JsonProperty("reblogged_root_avatar_url_512")]
        public string RebloggedRootAvatarUrl512 { get; set; }

        [JsonProperty("reblogged_root_avatar_url_128")]
        public string RebloggedRootAvatarUrl128 { get; set; }

        [JsonProperty("reblogged_root_avatar_url_96")]
        public string RebloggedRootAvatarUrl96 { get; set; }

        [JsonProperty("reblogged_root_avatar_url_64")]
        public string RebloggedRootAvatarUrl64 { get; set; }

        [JsonProperty("reblogged_root_avatar_url_48")]
        public string RebloggedRootAvatarUrl48 { get; set; }

        [JsonProperty("reblogged_root_avatar_url_40")]
        public string RebloggedRootAvatarUrl40 { get; set; }

        [JsonProperty("reblogged_root_avatar_url_30")]
        public string RebloggedRootAvatarUrl30 { get; set; }

        [JsonProperty("reblogged_root_avatar_url_24")]
        public string RebloggedRootAvatarUrl24 { get; set; }

        [JsonProperty("reblogged_root_avatar_url_16")]
        public string RebloggedRootAvatarUrl16 { get; set; }
    }
}