using System.Text.RegularExpressions;
using System.Collections.Generic;
using Newtonsoft.Json;
using Comments.Model;
using RestSharp;
using System;

namespace Comments
{
    class TumblrApi
    {
        public string EnterBlogName()
        {
            Console.WriteLine("Enter the Tumblr blog name:");
            return Console.ReadLine();
        }

        public string EnterPostRange()
        {
            Console.WriteLine("Enter the range:");
            return Console.ReadLine();
        }

        public List<int> ParseRange(string range)
        {
            List<int> parsedRange = new List<int>();
            string[] rangeValues = Regex.Split(range, @"\D+");

            foreach (var value in rangeValues)
            {
                parsedRange.Add(Int32.Parse(value));
            }

            return parsedRange;
        }

        public bool ValidateRange(int first, int second)
        {
            if (first > second || first < 0 || second < 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Performs `REST API` queries
        /// </summary>
        public RootObject GetBlogPosts(string blogName, int startIndex, int numberOfPosts = 50)
        {
            string baseUrl = $@"https://{blogName}.tumblr.com";
            string endPoint = @"/api/read/json";
            string parameters = $@"type=photo&num={numberOfPosts}&start={startIndex}";

            var client = CreateClient(baseUrl);
            var request = SendRequest(endPoint, parameters);
            var response = RecieveResponse(client, request);
            var jsonRootObject = DeserializeResponse(response);

            return jsonRootObject;
        }

        /// <summary>
        /// Client to translate RestRequests into Http requests and process response result
        /// </summary>
        private RestClient CreateClient(string baseUrl)
        {
            return new RestClient(baseUrl);
        }

        private RestRequest SendRequest(string endPoint, string parameters)
        {
            return new RestRequest($@"{endPoint}?{parameters}", Method.GET);
        }

        private IRestResponse RecieveResponse(RestClient client, RestRequest request)
        {
            return client.Execute(request);
        }

        /// <summary>
        /// Takes the response (which is in javascript) and converts it to a json string
        /// </summary>
        /// <param name='response'>The javascript response recieved from Tumblr</param>
        private RootObject DeserializeResponse(IRestResponse response)
        {
            string rawResponse = response.Content;
            string jsonString = "";

            for (int index = response.Content.IndexOf('{'); index <= response.Content.LastIndexOf('}'); index++)
            {
                jsonString += rawResponse[index];
            }

            var jsonRootObject = JsonConvert.DeserializeObject<RootObject>(jsonString);

            return jsonRootObject;
        }

        /// <summary>
        /// Prints basic info and photo URLs of the given blog
        /// </summary>
        public void ShowBlogDetails(int startIndex, int postNumber, RootObject jsonRootObject)
        {
            if (startIndex == 0)
            {
                ShowBlogInformation(jsonRootObject);
            }
            ShowPhotoUrls(jsonRootObject, postNumber);
        }

        private void ShowBlogInformation(RootObject jsonRootObject)
        {
            Console.WriteLine("Title: " + jsonRootObject.Tumblelog.Title);
            Console.WriteLine("Name: " + jsonRootObject.Tumblelog.Name);
            Console.WriteLine("Description: " + jsonRootObject.Tumblelog.Description);
            Console.WriteLine("No. of Posts: " + jsonRootObject.PostsTotal);
        }

        private void ShowPhotoUrls(RootObject jsonRootObject, int postNumber)
        {
            if (jsonRootObject.PostsTotal == 0)
            {
                return;
            }

            foreach (var post in jsonRootObject.Posts)
            {
                Console.Write($"{postNumber}. ");
                if (post.Photos.Count == 0)
                {
                    Console.WriteLine(post.PhotoUrl1280);
                }
                else
                {
                    foreach (var photo in post.Photos)
                    {
                        Console.WriteLine(photo.PhotoUrl1280);
                    }
                }
                postNumber += 1;
            }
        }
    }
}