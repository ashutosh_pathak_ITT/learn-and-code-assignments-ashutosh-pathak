using System.Collections.Generic;
using Newtonsoft.Json;

namespace Comments.Model
{
    public class RootObject
    {

        [JsonProperty("tumblelog")]
        public RootObjectTumblelog Tumblelog { get; set; }

        [JsonProperty("posts-start")]
        public int PostsStart { get; set; }

        [JsonProperty("posts-total")]
        public int PostsTotal { get; set; }

        [JsonProperty("posts-type")]
        public string PostsType { get; set; }

        [JsonProperty("posts")]
        public IList<Post> Posts { get; set; }
    }
}