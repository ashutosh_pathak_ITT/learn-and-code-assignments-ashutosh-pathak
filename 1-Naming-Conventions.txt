-------------- 1. Roll the dice-----------------

import random

def roll_dice(dice_sides):
    face_value = random.randint(1, dice_sides)
    return face_value


def main():
    dice_sides = 6
    play = True
    while play:
        choice = input("Ready to roll? Enter Q to Quit")
        if choice.lower() != "q":
            face_value = roll_dice(dice_sides)
            print("You have rolled a ", face_value)
        else:
            play = False



----------- 2. Guess correct number ------------

def check(num):
    if num.isdigit() and 1 <= int(num) <= 100:
        return True
    else:
        return False

def main():
    target = random.randint(1, 100)
    game_finished = False
    guess = input("Guess a number between 1 and 100:")
    no_of_guesses = 0
    while not game_finished:
        if not check(guess):
            guess = input("I won't count this one. Please enter a number between 1 to 100")
            continue
        else:
            no_of_guesses += 1
            guess = int(guess)

        if guess < target:
            guess = input("Too low. Guess again")
        elif guess > target:
            guess = input("Too High. Guess again")
        else:
            print("You guessed it in ", no_of_guesses, " guesses!")
            game_finished = True

main()



------------- 3. Armstrong number --------------

def armstrong(number):
    # Initializing Sum and Number of Digits
    sum = 0
    order = 0

    # Calculating Number of individual digits
    temp = number
    while temp > 0:
        order = order + 1
        temp = temp // 10

    # Finding Armstrong Number
    temp = number
    for _ in range(1, temp + 1):
        digit = temp % 10
        sum = sum + (digit ** order)
        temp //= 10
    return sum


# End of Function

# User Input
number = int(input("\nPlease Enter the Number to Check for Armstrong: "))

if (number == armstrong(number)):
    print("\n %d is Armstrong Number.\n" % number)
else:
    print("\n %d is Not a Armstrong Number.\n" % number)



-------------- 4. Selection Sort --------------

function selectionSort(array) {
  for (let i = 0; i < array.length; i++) {
    // Set default active minimum to current index.
    let minimum = i;
    // Loop to array from the current value.
    for (let j = i + 1; j < array.length; j++) {
      // If you find an item smaller than the current active minimum,
      // make the new item the new active minimum.
      if (array[j] < array[minimum]) {
        minimum = j;
      }
      // Keep on looping until you've looped over all the items in the array
      // in order to find values smaller than the current active minimum.
    }
    // If the current index isn't equal to the active minimum value's index anymore
    // swap these two elements.
    if (i !== minimum) {
      [array[i], array[minimum]] = [array[minimum], array[i]];
    }
  }
  return array;
}



------- 5. Find largest and smallest number -------

import java.util.Arrays;
/**
 * Java program to find largest and smallest number from an array.
 */
public class MaximumMinimumArrayDemo{
 
    public static void main(String args[]) {
        findnumber(new int[]{-20, 34, 21, -87, 92,
                             Integer.MAX_VALUE});
        findnumber(new int[]{10, Integer.MIN_VALUE, -2});
        findnumber(new int[]{Integer.MAX_VALUE, 40,
                             Integer.MAX_VALUE});
        findnumber(new int[]{1, -1, 0});
    }
 
    public static void findnumber(int[] numbers) {
        int largest = Integer.MIN_VALUE;
        int smallest = Integer.MAX_VALUE;
        for (int number : numbers) {
            if (number > largest) {
                largest = number;
            } else if (number < smallest) {
                smallest = number;
            }
        }
 
        System.out.println("Given integer array : " + Arrays.toString(numbers));
        System.out.println("Largest number in array is : " + largest);
        System.out.println("Smallest number in array is : " + smallest);
    }
}



--------- 6. Floor of mean of subarray -----------

using System;
using System.Numerics;

class MeanOfSubarray {
    static void Main(string[] args) {
       var numOfElementsAndQueries = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            var array = Array.ConvertAll(Console.ReadLine().Split(' '), long.Parse);
            long[] sumArray = new long[numOfElementsAndQueries[0] + 1];
            sumArray[0] = 0;
            for (int i = 1; i <= numOfElementsAndQueries[0]; i++)
            {
                sumArray[i] = sumArray[i - 1] + array[i - 1];
            }
            for (var i = 0; i < numOfElementsAndQueries[1]; i++)
            {
                var query = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
                Console.WriteLine((long)((long)(sumArray[query[1]] - sumArray[query[0] - 1]) / (query[1] - query[0] + 1)));
            }
    }
}