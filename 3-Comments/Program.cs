﻿using System.Collections.Generic;
using System;

namespace Comments
{
    class Program
    {
        static void Main(string[] args)
        {
            var tumblrApi = new TumblrApi();
            var parsedRange = new List<int>();
            
            string blogName = tumblrApi.EnterBlogName();
            string range = tumblrApi.EnterPostRange();

            try
            {
                parsedRange = tumblrApi.ParseRange(range);
            }
            catch (System.FormatException)
            {
                Console.WriteLine("Input string was of the wrong format. It should look like: number1-number2");
                return;
            }

            int rangeFrom = parsedRange[0];
            int rangeTo = parsedRange[1];

            bool isValidRange = tumblrApi.ValidateRange(rangeFrom, rangeTo);

            if (isValidRange)
            {
                int startIndex = rangeFrom - 1;
                int totalPosts = rangeTo - startIndex;
                int batches = totalPosts / 50;
                int remainingPosts = totalPosts % 50;
                int postNumber = 1;

                while(batches > 0)
                {
                    var jsonRootObject = tumblrApi.GetBlogPosts(blogName, startIndex);
                    tumblrApi.ShowBlogDetails(startIndex, postNumber, jsonRootObject);

                    startIndex += 50;
                    postNumber += 50;
                    batches -= 1;
                }

                if (remainingPosts != 0)
                {
                    var jsonRootObject = tumblrApi.GetBlogPosts(blogName, startIndex, remainingPosts);
                    tumblrApi.ShowBlogDetails(startIndex, postNumber, jsonRootObject);
                }
            }
            else
            {
                Console.WriteLine("Invalid Range!");
            }
        }
    }
}
