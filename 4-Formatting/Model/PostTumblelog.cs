using Newtonsoft.Json;
using System;

namespace Comments.Model
{
    public class PostTumblelog
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("cname")]
        public bool Cname { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("timezone")]
        public string Timezone { get; set; }

        [JsonProperty("avatar_url_512")]
        public Uri AvatarUrl512 { get; set; }

        [JsonProperty("avatar_url_128")]
        public Uri AvatarUrl128 { get; set; }

        [JsonProperty("avatar_url_96")]
        public Uri AvatarUrl96 { get; set; }

        [JsonProperty("avatar_url_64")]
        public Uri AvatarUrl64 { get; set; }

        [JsonProperty("avatar_url_48")]
        public Uri AvatarUrl48 { get; set; }

        [JsonProperty("avatar_url_40")]
        public Uri AvatarUrl40 { get; set; }

        [JsonProperty("avatar_url_30")]
        public Uri AvatarUrl30 { get; set; }

        [JsonProperty("avatar_url_24")]
        public Uri AvatarUrl24 { get; set; }

        [JsonProperty("avatar_url_16")]
        public Uri AvatarUrl16 { get; set; }
    }
}